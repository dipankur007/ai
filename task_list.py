l=[]
while True:
    print("Press 1 to add an element to list\nPress 2 to Delete element from list\nPress 3 to update an element in the list\nPress 4 to view the list\nPress 5 to Exit")    
    e=int(input("Enter your choice : "))
    if e==1 :
        print("Type Exit to stop adding elements to list : ")
        while True:
            
            x=input('enter element : ').capitalize()
            if(x=='Exit'):
                break 
            l.append(x)
        print('\n')        
    elif e==2: 
        print("Type Exit to stop deleting elements from list : ")
        print('Elements in the list are : ' , l)
        while True: 
            print('\n')
            ele=input('Enter element to delete from list : ').capitalize()
            if(ele=='Exit'):
                break
            elif ele in l:
                l.remove(ele)
                print('Element removed successfully !!')
            else:
                print('Element not in list.')   
        print('\n')                 
    elif e==3:
        print("Type Exit to stop updating elements of list : ")
        print('Elements in the list are : ' , l)
        while True: 
            print('\n')
            element=input('Enter element to Update : ').capitalize()
            if(element=='Exit'):
                break
            elif element in l:
                n=l.index(element) 
                ne=input('Enter new element replacing '+'\''+element+'\' : ').capitalize()
                l[n]=ne
                print('List Updated Successfully!!!')
            else:
                print('Element not in list.')
        print('\n')        
    elif e==4:
        for i in range(0,len(l)):
            print(i+1,':',l[i])
        print('Total users :' , len(l))    
        print('\n')
    elif e==5:
        break
    else:
        print('please choose a valid option')
        print('\n')            
