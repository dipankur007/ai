from functools import reduce

#*******F.I.L.T.E.R.*******
marks=[100,97,7,20,33,88,22,9,79,93]

def check(v):
    if v>=33:
        return True
result=list(filter(check,marks))
print(result)
print("Total : ",len(result))            

#using lambda
for i in marks:
    c=lambda i:i>=33
    result=list(filter(c,marks))
print(result)
print("Total : ",len(result))        

#using lambda comprehension
lmd=list(filter(lambda x:x>=33,marks))
print(lmd)


#*******M.A.P*******
#eg1:
ls=["DiPankUr" , "HiMMat" , "harSH"]
def check1(st):
    return st.title()


res=list(map(check1,ls))
print(res) 

#using comprehension and lambda
lmd2=list(map(lambda y:y.title(),ls))
print(lmd2)

#eg2:
def perc(n):
    p=(n/120)*100
    return round(p,3)
percentage=list(map(perc,marks))
print(percentage)


#*******R.E.D.U.C.E.*******
#eg1:
r=reduce(lambda a,b:a+b, marks)
total=len(marks)*100
print("Avg total : " ,(r/total)*100)

#eg2;
max=reduce(lambda w,x:w if w>x else x,marks)
print(max)